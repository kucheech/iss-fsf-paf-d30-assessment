(function () {
    "use strict";
    angular.module("MyApp", ["ui.router", "http-auth-interceptor", "ngFileUpload", "ui.bootstrap", "components", "ui.toggle"]);
})();