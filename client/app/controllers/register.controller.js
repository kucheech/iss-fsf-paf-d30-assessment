(function () {
    "use strict";
    angular.module("MyApp").controller("RegisterCtrl", RegisterCtrl);

    RegisterCtrl.$inject = ["MyAppService", "$state", "$timeout"];

    function RegisterCtrl(MyAppService, $state, $timeout) {
        var vm = this; // vm
        vm.progress = 0;
        vm.interviewers = [];
        vm.interviewer = null;
        vm.user = {
            name: "",
            email: ""
        };
        vm.status = false;
        vm.message = "";
        vm.showMessage = false;
        vm.file = "";

        vm.setProgress = setProgress
        vm.setProgress2 = setProgress2
        // vm.incProgress = incProgress
        vm.cancel = cancel;
        vm.register = register;
        vm.showPage = showPage;
        vm.validateEmail = validateEmail;
        vm.gotoPage = gotoPage;

        init();

        /////////////////////////

        function gotoPage(p, next) {
            switch(parseInt(p)) {
                case 0: vm.progress = next ? 0 : 0 ; return;
                case 1: vm.progress = next ? 5 : 8 ; return;
                case 2: vm.progress = next ? 10 : 27.5 ; return;
                case 3: vm.progress = next ? 30 : 55 ; return;
                case 4: vm.progress = next ? 60 : 55 ; return;
            }
        }

        function validateEmail() {
            vm.message = null;
            vm.showMessage = false;

            MyAppService.validateEmail(vm.user.email)
                .then(function (result) {                    
                    // console.log(result);
                    if(result.status == 200) {
                        vm.progress = 5;
                    } 
                }).catch(function (err) {
                    // console.log(err);
                    if(err.status == 302) {
                        vm.message = err.data;
                        vm.showMessage = true;
                    }

                });
        }
        
        function showPage(p) {
            switch(parseInt(p)) {
                case 0: return vm.progress == 0;
                case 1: return vm.progress > 0 && vm.progress < 10;
                case 2: return vm.progress >= 10 && vm.progress < 30;
                case 3: return vm.progress >= 30 && vm.progress < 60
                case 4: return vm.progress >= 60;
            }
        }

        function setProgress2(n) {
            vm.progress = n;
        }

        function setProgress(n) {
            // progress is only incremented
            if (n > vm.progress) {
                vm.progress = n;
            }
            // console.log(vm.progress);
        }

        // function incProgress(max) {
        //     // progress is only incremented
        //     if (vm.progress < max) {
        //         vm.progress += 2.5;
        //     }
        //     // console.log(vm.progress);
        // }

        function init() {
            MyAppService.getInterviewers()
                .then(function (result) {
                    vm.interviewers = result;
                    // console.log(result);  
                    vm.user.interviewer = 0;                  
                }).catch(function (err) {
                    console.log(err);
                });
        }

        function cancel() {
            $state.go("home");
        }

        function register() {
            vm.showMessage = false;

            MyAppService.submitRegistration(vm.user, vm.file)
                .then(function (result) {
                    console.log(result);
                    if (result.status == 201) {
                        vm.status = true;
                        vm.message = result.data;
                        vm.showMessage = true;

                        $timeout(function () {
                            $state.go("home");
                        }, 3000);
                    }
                    // $state.go("login");
                }).catch(function (err) {
                    console.log(err);
                    vm.status = false;
                    vm.message = err.data;
                    vm.showMessage = true;
                });
        }

    }

})();