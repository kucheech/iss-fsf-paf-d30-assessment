(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider", "$locationProvider"];

    function MyConfig($stateProvider, $urlRouterProvider, $locationProvider) {
        $locationProvider.html5Mode(true);

        $stateProvider
            .state("home", {
                url: "/",
                templateUrl: "/views/home.html",
                // controller: "HomeCtrl",
                // controllerAs: "ctrl"
            })
            .state("login1", {
                url: "/login",
                templateUrl: "/views/login.html",
                controller: "LoginCtrl",
                controllerAs: "ctrl"
            })
            .state("login2", {
                url: "/admin/login",
                templateUrl: "/views/admin/login.html",
                controller: "InterviewerLoginCtrl",
                controllerAs: "ctrl"
            })
            .state("protected", {
                url: "/protected",
                templateUrl: "/views/protected.html",
                controller: "ProtectedCtrl",
                controllerAs: "ctrl"
            })
            .state("candidates", {
                url: "/admin/candidates",
                templateUrl: "/views/admin/candidates.html",
                controller: "CandidatesCtrl",
                controllerAs: "ctrl"
            })
            .state("edit", {
                url: "/edit/:id",
                templateUrl: "/views/edit.html",
                controller: "EditCtrl",
                controllerAs: "ctrl"
            })
            .state("editcandidatedata", {
                url: "/editCandidateData/:id",
                templateUrl: "/views/admin/editcandidatedata.html",
                controller: "EditCandidateDataCtrl",
                controllerAs: "ctrl"
            })
            .state("register", {
                url: "/register",
                templateUrl: "/views/register.html",
                controller: "RegisterCtrl",
                controllerAs: "ctrl"
            })
            .state("resetpassword", {
                url: "/resetpassword",
                templateUrl: "/views/resetpassword.html",
                controller: "ResetPasswordCtrl",
                controllerAs: "ctrl"
            })

        $urlRouterProvider.otherwise("/home");
    }





})();