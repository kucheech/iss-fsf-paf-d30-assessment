### server unit testing
1. ```npm install --save-dev babel-cli babel-preset-env jest supertest superagent```
1. change test in package.json to jest
1. ```npm test```

### Kill node window process
http://www.wisdomofjim.com/blog/how-kill-running-nodejs-processes-in-windows
```taskkill /im node.exe /F```

### auth local
```npm install passport passport-local body-parser express-session connect-ensure-login --save```

### HTTP Auth Interceptor Module
https://github.com/witoldsz/angular-http-auth
```bower install --save angular-http-auth```

### heroku link
https://agile-headland-99281.herokuapp.com/  
shortened url: https://goo.gl/iB2aLw

### AWS link
restarted instance and obtained a new ip address
http://13.229.105.67

### local run
1. ```npm install```
1. Create a .env file with the following environment variables
```
DB_NAME = "focus"
DB_USER = <removed>
DB_PASSWORD = <removed>
DB_HOST = "localhost"

MG_API_KEY = <removed>
MG_DOMAIN = <removed>
```